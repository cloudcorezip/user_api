const { faker } = require('@faker-js/faker');
const { User } = require('../../../src/models');
const mongoose  = require('mongoose');

describe('User model', () => {
  describe('User validation', () => {
    let newUser;
    beforeEach(() => {
      newUser = {
        name: faker.person.fullName(),
        email: faker.internet.email().toLowerCase(),
        password: faker.string.alpha(10),
        address: faker.string.alpha(10),
        cityId: new mongoose.Types.ObjectId('648acb4e69b9515a0b4694b4'),
        hobbies: [faker.string.alpha(10), faker.string.alpha(10)],
      };
    });

    test('should correctly validate a valid user', async () => {
      await expect(new User(newUser).validate()).resolves.toBeUndefined();
    });

    test('should throw a validation error  if cityId invalid', async () => {
      newUser.cityId = '1177aac012';
      await expect(new User(newUser).validate()).rejects.toThrow();
    });

    test('should throw a validation error  if email invalid', async () => {
      newUser.email = 'test@foo ';
      await expect(new User(newUser).validate()).rejects.toThrow();
    });

    test('should throw a validation error  if empty', async () => {
      newUser = {};
      await expect(new User(newUser).validate()).rejects.toThrow();
    })
  })
})