const { faker } = require('@faker-js/faker');
const { City } = require('../../../src/models');

describe('City model', () => {
  describe('City validation', () => {
    let newCity;
    beforeEach(() => {
      newCity = {
        name: faker.string.alpha(),
      };
    });

    test('should correctly validate a valid user', async () => {
      await expect(new City(newCity).validate()).resolves.toBeUndefined();
    });

    test('should throw validation error if empty', async () => {
      newCity = {};
      await expect(new City(newCity).validate()).rejects.toThrow();
    });
  })
})