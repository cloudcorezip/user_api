const mongoose = require('mongoose');
const { toJSON } = require('../../../../src/models/plugins');

describe('toJSON plugin', () => {
  let conn;

  beforeEach(() => {
    conn = mongoose.createConnection();
  });

  it('should replace _id with id', () => {
    const schema = mongoose.Schema();
    schema.plugin(toJSON);
    const Model = conn.model('Model', schema);
    const doc = new Model();
    expect(doc.toJSON()).not.toHaveProperty('_id');
    expect(doc.toJSON()).toHaveProperty('id', doc._id.toString());
  })

  it('should remove any private', () => {
    const schema = mongoose.Schema({
      public: { type: String },
      private: { type: String, private: true },
    });
    schema.plugin(toJSON);
    const Model = conn.model('Model', schema);
    const doc = new Model({ public: 'public', private: 'private' });
    expect(doc.toJSON()).not.toHaveProperty('private');
    expect(doc.toJSON()).toHaveProperty('public');
  });

  it('should remove any nested private', () => {
    const schema = mongoose.Schema({
      public: { type: String },
      private: { type: String, private: true },
    });
    schema.plugin(toJSON);
    const Model = conn.model('Model', schema);
    const doc = new Model({ public: 'public', parent: {private: 'private' }});
    expect(doc.toJSON()).not.toHaveProperty('private');
    expect(doc.toJSON()).toHaveProperty('public');
  });
});
