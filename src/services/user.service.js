const httpStatus = require('http-status');
const mongoose = require('mongoose');
const { User } = require('../models');
const ApiError = require('../utils/ApiError');
const moment = require('moment');

const createUser = async (userBody) => {
  if (await User.isEmailTaken(userBody.email)) {
    throw new ApiError(httpStatus.FORBIDDEN, 'Email Already taken');
  }
  return User.create(userBody);
}

const getUserByEmail = async (email) => {
  return User.findOne({ email });
}

const updateLastLogin = async (email) => {
  const update = { last_login: new Date() }
  await User.findOneAndUpdate({email}, update);
}

const getAllUsers = async (filter, options) => {
  const users = await User.paginate(filter, options);
  return users;
}

const getUserById = async (id) => {
  const user = await User.findByIdDetail(id);
  if (!user[0]) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }

  user[0].id = user[0]._id.toString();
  user[0].createdAt = moment(user.createdAt).format('Y-MM-d H:mm:ss');
  user[0].updatedAt = moment(user.updatedAt).format('Y-MM-d H:mm:ss');
  user[0].last_login = moment(user.last_login).format('Y-MM-d H:mm:ss');
  delete user[0]._id;
  
  return user[0];
}

const updateUserPassword = async (body) => {
  const user = await getUserByEmail(body.email);
  if (!user) {
    throw new ApiError(httpStatus.FORBIDDEN, 'Email not found');
  }

  if(user.password !== body.current_password) {
    throw new ApiError(httpStatus.FORBIDDEN, 'Current password is wrong');
  }

  await User.findOneAndUpdate({"email": user.email}, { password: body.new_password})
  return user;
}

const deleteUserById = async (userId) => {
  const user = await User.findOne({_id: userId});
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  await user.deleteOne({ _id: user._id});
  return user;
}

module.exports = {
  createUser,
  getUserByEmail,
  getAllUsers,
  updateLastLogin,
  getUserById,
  updateUserPassword,
  deleteUserById
};