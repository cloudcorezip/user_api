const { City } = require('../models');

const getAllCities = async (filter, options) => {
  const cities = await City.paginate(filter, options);
  return cities;
}

module.exports = {
  getAllCities
};