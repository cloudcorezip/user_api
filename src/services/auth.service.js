const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const { userService } = require('.');


const loginUser = async (email, password) => {
  const user = await userService.getUserByEmail(email);
  if(!user || !(await user.isPasswordMatch(password))) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Incorrect email or password')
  }
  await userService.updateLastLogin(user.email);
  return user;
}

module.exports = {
  loginUser
}