const catchAsync = require('../utils/catchAsync');
const pick = require('../utils/pick');
const { cityService } = require('../services');

const getCities = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const results = await cityService.getAllCities(filter, options)
  res.send(results);
})

module.exports = {
  getCities
}