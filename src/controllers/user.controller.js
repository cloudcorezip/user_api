const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const pick = require('../utils/pick');
const { userService } = require('../services');

const getUsers = catchAsync(async (req,res) => {
  const filter = pick(req.query, ['name']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const results = await userService.getAllUsers(filter, options)
  res.send(results);
})

const getUser = catchAsync(async (req, res) => {
  const user = await userService.getUserById(req.params.userId);
  res.send(user);
})

const deleteUser = catchAsync(async (req,res) => {
  await userService.deleteUserById(req.params.userId);
  res.status(httpStatus.CREATED).send();
})

const updatePassword = catchAsync(async (req, res) => {
  await userService.updateUserPassword(req.body);
  res.status(httpStatus.CREATED).send();
})

module.exports = {
  getUsers,
  getUser,
  deleteUser,
  updatePassword
}