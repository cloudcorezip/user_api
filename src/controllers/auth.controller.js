const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { userService, authService } = require('../services');

const register = catchAsync(async (req, res) => {
  await userService.createUser(req.body);
  res.status(httpStatus.CREATED).send();
})

const login = catchAsync(async (req, res) => {
  const { email, password } = req.body;
  await authService.loginUser(email, password);
  res.send();
})

module.exports = {
  register,
  login
}