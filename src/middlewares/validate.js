const Joi = require('joi');
const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const { ValidationError } = require('joi')

const validate = (schema) => (req, res, next) => {
  const validSchema = pick(schema, ['params', 'query', 'body']);
  const object = pick(req, Object.keys(validSchema));
  const { value, error } = Joi.compile(validSchema)
    .prefs({ errors: { label: 'key' }, abortEarly: false })
    .validate(object);

  if (error) {
    // if (error instanceof ValidationError ) {
      console.log(error);
      let tempArr = [];
      error.details.forEach(x => {
        tempArr.push({
          message: x.message,
          context: x.context.label
        })
      })
      return res.status(httpStatus.FORBIDDEN).json(tempArr);
    // }

    // const errorMessage = error.details.map((details) => details.message).join(', ');
    // return next(new ApiError(httpStatus.FORBIDDEN, errorMessage));
  }
  Object.assign(req, value);
  return next();
};

module.exports = validate;