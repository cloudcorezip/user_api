const express = require('express');
const mongoSanitize = require('express-mongo-sanitize');
const httpStatus = require('http-status');
const config = require('./config/config');
const { errorConverter, errorHandler } = require('./middlewares/error');
const ApiError = require('./utils/ApiError');

// routes
const routes = require('./routes/v1');

const app = express();

app.use(express.json());

app.use(express.urlencoded({ extended: true}));

app.use(mongoSanitize());

app.use('/v1', routes)

app.get('/', (_, res) => {
  res.send('bruh');
})

app.use((req, res, next) => {
  next(new ApiError(httpStatus.NOT_FOUND, 'Not found'));
});

app.use(errorConverter);
app.use(errorHandler);

module.exports = app;