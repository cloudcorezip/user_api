const Joi = require('joi');
const { objectId} = require('./custom.validation');

// move to custom validation rename the function 
// to global get all validation or smthn
const getUsersValidation = {
  query: Joi.object().keys({
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
    sortBy: Joi.string(),
  }),
};

const getUserByIdValidation = {
  params: Joi.object().keys({
    userId: Joi.string().custom(objectId)
  }),
}

const changePasswordValidation = {
  body: Joi.object().keys({
    email: Joi.string().email().required(),
    current_password: Joi.string().required(),
    new_password: Joi.string()
      .required()
      .disallow(Joi.ref('current_password'))
      .messages({ 
        "any.invalid": "New password mustn't same with current password" 
      }),
  }),
}

module.exports = {
  getUsersValidation,
  getUserByIdValidation,
  changePasswordValidation
}
