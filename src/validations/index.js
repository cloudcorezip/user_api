module.exports.authValidation = require('./auth.validation');
module.exports.userValidation = require('./user.validation');
module.exports.cityValidation = require('./city.validation');
