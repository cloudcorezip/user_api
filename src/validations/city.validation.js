const Joi = require('joi');

const getCitiesValidation = {
  query: Joi.object().keys({
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
    sortBy: Joi.string(),
    name: Joi.string()
  }),
};

module.exports = {
  getCitiesValidation,
}