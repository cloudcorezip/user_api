const Joi = require('joi');

const registerValidation = {
  body: Joi.object().keys({
    email: Joi.string().required().email(),
    password: Joi.string().required(),
    confirm_password: Joi.string().valid(Joi.ref('password'))
      .required()
      .label('Confirm password')
      .options({ messages: { 'any.only': '{{#label}} does not match with password'}}),
    name: Joi.string().required(),
    address: Joi.string().required(),
    cityId: Joi.string().required(),
    hobbies: Joi.array().items(Joi.string().required()).required()
  })
}

const loginValidation = {
  body: Joi.object().keys({
    email: Joi.string().required().email(),
    password: Joi.string().required()
  })
}

module.exports = {
  registerValidation,
  loginValidation
};