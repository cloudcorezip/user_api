const express = require('express');
const { userController } = require('../../controllers');
const { userValidation } = require('../../validations');
const validate = require('../../middlewares/validate');

const router = express.Router();

router.get('/users', validate(userValidation.getUsers), userController.getUsers);

router
  .route('/user/:userId')
  .get(validate(userValidation.getUserByIdValidation), userController.getUser)
  .delete(validate(userValidation.getUserByIdValidation), userController.deleteUser);

router.patch('/user', validate(userValidation.changePasswordValidation), userController.updatePassword);

module.exports = router;