const express = require('express');
const validate  = require('../../middlewares/validate')
const { cityContoroller } = require('../../controllers');
const { cityValidation } = require('../../validations');

const router = express.Router();

router.get('/cities', validate(cityValidation.getCitiesValidation),  cityContoroller.getCities);


module.exports = router;