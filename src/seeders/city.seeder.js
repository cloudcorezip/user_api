const City = require('../models/city.model');
const mongoose = require('mongoose');
const config = require("../config/config");

const city = [
  new City({
    name: 'City A'
  }),
  new City({
    name: 'City B'
  }),
  new City({
    name: 'City C'
  }),
  new City({
    name: 'City D'
  }),
];

mongoose.connect(config.mongoose.url, {})
  .catch(err => {
    console.log(err);
    process.exit(1);
  })
  .then(() => {
    console.log("connected to db");
  })

city.map((c, i) => {
  c.save().then(() => {
    if (i === city.length - 1) {
      console.log("DONE");
      mongoose.disconnect();
    }
  }).catch((err) => {
    console.log(err);
    process.exit(1);
  })
});