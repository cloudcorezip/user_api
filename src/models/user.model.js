const mongoose = require('mongoose');
const validator = require('validator');
const { toJSON, paginate } = require('./plugins');


const userSchema = mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
      validate(val) {
        if (!validator.isEmail(val)) {
          throw new Error('Invalid email');
        }
      },
    },
    password: {
      type: String,
      required: true,
      trim: true,
      private: true,
    },
    name: {
      type: String,
      required: true,
      trim: true,
    },
    address: {
      type: String,
      required: true,
      trim: true,
    },
    cityId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'cities',
      required: true,
    },
    last_login: {
      type: Date,
      default: '',
    },
    hobbies: {
      type: [String],
      required: true,
    },
  },
  {
    timestamps: true
  },
);

userSchema.plugin(toJSON);
userSchema.plugin(paginate);

userSchema.statics.isEmailTaken = async function (email, excludeUserId) {
  const user = await this.findOne({ email, _id: { $ne: excludeUserId } });
  return !!user;
};

userSchema.statics.findByIdDetail = async function (id) {
  const user = this;
  return await user.aggregate([{
    $lookup: {
      from: "cities",
      localField: 'cityId',
      foreignField: '_id',
      as: "city"
    }
  },
  { $unwind: "$city" },
  {
    $set: {
      "profile": {
        name: "$name",
        address: "$address",
        city: {
          id: { $toString: "$cityId" },
          name: "$city.name"
        },
        hobbies: "$hobbies"
      },
      name: "$$REMOVE",
      address: "$$REMOVE",
      city: "$$REMOVE",
      hobbies: "$$REMOVE",
      cityId: "$$REMOVE",
      password: "$$REMOVE",
      __v: "$$REMOVE",
    }
  },
  {
    $match: { "_id": new mongoose.Types.ObjectId(id) }
  }])
}

userSchema.methods.isPasswordMatch = async function (password) {
  const user = this;
  return password === user.password;
};


const User = mongoose.model('User', userSchema);

module.exports = User;