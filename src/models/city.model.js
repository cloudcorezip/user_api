const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins')

const citySchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
  },
);

citySchema.plugin(toJSON);
citySchema.plugin(paginate);

const City = mongoose.model('City', citySchema);

module.exports = City;