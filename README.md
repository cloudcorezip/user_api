# User API

### Run Manual
```sh
$ yarn
$ yarn dev
```

### RUN USING DOCKER
```sh
$ yarn docker:dev # development
$ yarn docker:test # Test
```


#### MISC
- Database dump in ``dump`` folder
